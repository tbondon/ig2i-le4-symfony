<?php

namespace App\Entity;

use DateTime;

class Article
{
	// Properties
	private $title = "title";
	private $subtitle = "subtitle";
	private $createdAt;
	private $author = "author";
	private $body = "body";
	private $image = "url";

	// Constructor
	public function Article()
	{
		$title = new DateTime();
	}

	// Getters
	public function getTitle(): string
	{
		return $this->title;
	}

	public function getSubtitle(): string
	{
		return $this->subtitle;
	}

	public function getCreatedAt(): DateTime
	{
		return $this->createdAt;
	}

	public function getAuthor(): string
	{
		return $this->author;
	}

	public function getBody(): string
	{
		return $this->body;
	}

	public function getImage(): string
	{
		return $this->image;
	}

	// Setters
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	public function setSubtitle(string $subtitle)
	{
		$this->subtitle = $subtitle;
	}

	public function setCreatedAt(DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
	}

	public function setAuthor(string $author)
	{
		$this->author = $author;
	}

	public function setBody(string $body)
	{
		$this->body = $body;
	}

	public function setImage(string $image)
	{
		$this->image = $image;
	}
}
