<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use DateTime;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            $comment = new Comment();
            $comment->setName($faker->name);
            $comment->setEmail($faker->email);
            $comment->setCreatedAt(new DateTime());
            $comment->setComment($faker->sentence($nbWords = 16, $variableNbWords = true));
            $comment->setArticle($this->getReference(AppFixtures::ARTICLE));
            $manager->persist($comment);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            AppFixtures::class,
        );
    }
}
