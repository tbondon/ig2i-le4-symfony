<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use DateTime;
use Faker;

class AppFixtures extends Fixture
{
    public const ARTICLE = "article";

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $article = new Article();
        $article->setTitle($faker->sentence($nbWords = 4, $variableNbWords = true));
        $article->setSubtitle($faker->sentence($nbWords = 16, $variableNbWords = true));
        $article->setCreatedAt(new DateTime());
        $article->setAuthor($faker->name);
        $article->setBody($faker->text);
        $article->setImage($faker->imageUrl);
        $manager->persist($article);

        $manager->flush();

        $this->addReference(self::ARTICLE, $article);
    }
}
