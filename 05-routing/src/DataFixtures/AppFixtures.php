<?php

namespace App\DataFixtures;

use Faker;
use DateTime;
use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            $article = new Article();
            $article->setTitle($faker->sentence($nbWords = 4, $variableNbWords = true));
            $article->setSubtitle($faker->sentence($nbWords = 16, $variableNbWords = true));
            $article->setCreatedAt(new DateTime());
            $article->setAuthor($faker->name);
            $article->setBody($faker->text);
            $article->setImage($faker->imageUrl);
            $manager->persist($article);
        }


        $manager->flush();
    }
}
