<?php

require __DIR__.'/vendor/autoload.php';

use App\Command\HelloWorldCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new HelloWorldCommand());

$application->run();
